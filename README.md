Building Pharo Static Spur32
============================
Build status · [![build status](https://gitlab.com/johnnyzz/pharo-build-static/badges/master/build.svg)](https://gitlab.com/johnnyzz/pharo-build-static/commits/master)
```sh
 git clone --depth=1 git@gitlab.com:johnnyzz/pharo-vm.git pharo-vm/
 cd pharo-vm
 git submodule init
 git submodule update --recursive --remote
 cd ..
 mkdir pharo-build-static
 cd pharo-build-static
 cmake ../pharo-vm/ -DST_SCRIPT=gen-pharo-unix32-static
 make
 ```